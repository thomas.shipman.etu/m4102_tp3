package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDAO;
import fr.ulille.iut.pizzaland.dao.PizzaDAO;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class PizzaResourceTest extends JerseyTest{

	private PizzaDAO dao;
	private IngredientDAO daoIng;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDAO.class);
		dao.createTablePizzaIngredients();
		daoIng = BDDFactory.buildDao(IngredientDAO.class);
		daoIng.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>(){});
		assertEquals(0, pizzas.size());
	}

	@Test
	public void testGetExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chevre");

		long id = dao.insert(pizza.getName());
		pizza.setId(id);

		Response response = target("/pizzas/" + id).request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}
	
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas/12").request().get();	
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	//TODO réparer erreur
	@Test
	public void testGetIngredients() {
		Pizza pizza = new Pizza();
		pizza.setName("Chevre");
		long idIng = daoIng.insert("Chevre");
		long id = dao.insert(pizza.getName());
		pizza.setId(id);
		dao.insertIngredient(pizza.getId(), idIng);

		String target = "/pizzas/" + id +"/ingredients";
		Response response = target(target).request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}
	
	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
        pizza.setName("Chèvre");
        long id = dao.insert(pizza.getName());

        Response response = target("pizzas/" + id + "/name").request().get();

        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        assertEquals("Chèvre", response.readEntity(String.class));
	}
	
	//TODO réparer erreur
	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chevre");

		long id = dao.insert(pizza.getName());
		pizza.setId(id);

		Response response = target("/pizzas/" + id).request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		
		response = target("/pizzas/" + id).request().delete();
		
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response);
		
		Pizza result = dao.findById(id);
    	assertEquals(result, null);
	}
	
	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
        pizzaCreateDto.setName("Chèvre");

        Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

        assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());

        assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

}
