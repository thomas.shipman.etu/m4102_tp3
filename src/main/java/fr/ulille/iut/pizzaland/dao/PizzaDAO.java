package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDAO {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createTablePizza();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzasIngredients (idPizza INTEGER, idIngredient INTEGER, CONSTRAINT pk_pizzasIngredients PRIMARY KEY (idPizza,idIngredient))")
	void createTableIngredients();
	
	@Transaction
	default void createTablePizzaIngredients() {
		createTablePizza();
		createTableIngredients();
		
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropTable();

	@SqlUpdate("INSERT INTO pizzas (name) VALUES (:name)")
	@GetGeneratedKeys
	long insert(String name);
	
	@SqlUpdate("INSERT INTO pizzasIngredients VALUES (:idPizza,:idIngredient)")
	void insertIngredient(long idPizza, long idIngredient);

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();
	
	@SqlQuery("SELECT * FROM pizzasIngredients WHERE idPizza = :idPizza")
	List<Ingredient> getIngredient(long idPizza);

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);
	
	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);
	
	@SqlUpdate("DELETE FROM pizzasIngredients WHERE idPizza = :idPizza")
	void removeFromPizzasIngredients(long idPizza);
	
	@SqlUpdate("DELETE FROM pizzas WHERE idPizza = :idPizza")
	void removeFromPizzas(long idPizza);

}
