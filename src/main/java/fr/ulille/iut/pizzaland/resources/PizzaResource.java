package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDAO;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

@Path("/pizzas")
public class PizzaResource {
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzaDAO dao;

	@Context
	public UriInfo uriInfo;

	public PizzaResource() {
		dao = BDDFactory.buildDao(PizzaDAO.class);
	    dao.createTablePizzaIngredients();
	}

	@GET
	public List<PizzaDto> getAll() {

		LOGGER.info("IngredientResource:getAll");

	    List<PizzaDto> l = dao.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
	    return l;
	}
	
	@GET
	@Path("{id}/name")
	public String getPizzaName(@PathParam("id") long id) {
		LOGGER.info("getPizzaName("+id+")");
		Pizza pizza = dao.findById(id);
		if(pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return pizza.getName();
	}
	
	@GET
	@Path("{id}")
	public PizzaDto getOnePizza(@PathParam("id") long id) {
		LOGGER.info("getOnePizza(" + id + ")");
		/*try {
			Pizza pizza = pizzas.findById(id);
			return Pizza.toDto(pizza);
		} catch(Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}	*/
		Pizza pizza = dao.findById(id);
		if(pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return Pizza.toDto(pizza);
	}
	
	@GET
	@Path("{id}/ingredients)")
	public List<Ingredient> getIngredients(@PathParam("id") long id){
		LOGGER.info("getIngredients(" + id + ")");
		try {
			return dao.getIngredient(id);
		} catch(Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deletePizza(@PathParam("id") long id){
		LOGGER.info("deletePizza("+id+")");
		if(dao.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		dao.removeFromPizzas(id);
		
		return Response.status(Status.ACCEPTED).build();
	}
	
	@POST
	@Path("{id}")
	public Response createPizza(PizzaCreateDto dtoCreate) {
		 Pizza existing = dao.findByName(dtoCreate.getName());
	        if ( existing != null ) {
	            throw new WebApplicationException(Response.Status.CONFLICT);
	        }
	        
	        try {
	            Pizza pizza = Pizza.fromPizzaCreateDto(dtoCreate);
	            long id = dao.insert(pizza.getName());
	            pizza.setId(id);
	            PizzaDto pizzaDto = Pizza.toDto(pizza);

	            URI uri = uriInfo.getAbsolutePathBuilder().path("" + id).build();

	            return Response.created(uri).entity(pizzaDto).build();
	        }
	        catch ( Exception e ) {
	            e.printStackTrace();
	            throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
	        }
	}

}
