package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private long id;
	private String name;
	private List<Ingredient> ingredients = new ArrayList<Ingredient>();
	
	public Pizza(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Pizza() {
		
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}

	public void addIngredient(Ingredient ingredient) {
		this.ingredients.add(ingredient);
	}
	
	private void setIngredient(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza pizza) {
	    PizzaDto dto = new PizzaDto();
	    dto.setId(pizza.getId());
	    dto.setName(pizza.getName());

	    return dto;
	  }

	  public static Pizza fromDto(PizzaDto dto) {
	    Pizza pizza = new Pizza();
	    pizza.setId(dto.getId());
	    pizza.setName(dto.getName());

	    return pizza;
	  }
	  
	  @Override
	  public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Pizza other = (Pizza) obj;
	    if (id != other.id)
	        return false;
	    if (name == null) {
	        if (other.name != null)
	            return false;
	    } else if (!name.equals(other.name))
	        return false;
	    return true;
	  }

	  @Override
	  public String toString() {
	    return "Pizza [id=" + id + ", name=" + name + "]";
	  }

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dtoCreate) {
		Pizza pizza = new Pizza();
		pizza.setName(dtoCreate.getName());
		pizza.setIngredient(dtoCreate.getIngredients());
		return pizza;
	}
}

